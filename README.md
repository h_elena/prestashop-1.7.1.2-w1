# README #
This payment driver is designed to receive payment orders for CMS Prestoshop

=== Wallet One Checkout ===
Contributors: h-elena
Version: 3.1.0
Requires at least: 1.7.1.2
Tested up to: 1.7.1.2
Stable tag: 1.7.1.2
License: GPLv3
License URI: http://www.gnu.org/licenses/gpl-3.0.en.html

The Wallet One Checkout module is a payment system for CMS Prestoshop.

== Description ==
If you have an online store, then you need a module payments for orders made.

== Installation ==
1. Register on the site https://www.walletone.com/merchant/client/#/signup
2. Download the module on the repository .
3. Instructions for configuring the module is in the file read.pdf.

== Screenshots ==
Screenshots are to be installed in the file read.pdf

== Changelog ==

= 1.0.1 =
* Fix - Fix creating order and clear cart

= 2.0.0 =
* Fix - Add universals classes and new settings

= 2.0.1 =
* Fix - fixed bug with checkin signature in codding string windows-1251

= 2.1.0 =
* Added support for new version 1.7.0.0

= 3.0.0 =
* Fix - fixed bug with anser from calback payment system

= 3.1.0 =
* Added new currency AZN and support for new version cms 1.7.1.2

== Frequently Asked Questions ==
No recent asked questions
